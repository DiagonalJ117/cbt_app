import 'package:flutter/material.dart';
class LogBody extends StatefulWidget{
  @override
  _LogBodyState createState() => new _LogBodyState();
}

class _LogBodyState extends State<LogBody>{
    var _index = 0;
  static var _focusNode = new FocusNode();
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  //static MyData data = new MyData();
  List<Step> steps;

  
    @override
  void initState() {
    super.initState();
    _focusNode.addListener(() {
      setState(() {});
      print('Has focus: $_focusNode.hasFocus');
    });

 steps = [
    Step(
          title: Text("Situation"),
          content: _situationQs(),
          isActive: true,
          state: StepState.indexed
        ),
        Step(
          title: Text("Criterion 1"),
          content: _criterion1Qs(),
          isActive: true,
          state: StepState.indexed
        ),
        Step(
          title: Text("Criterion 2"),
          content: _criterion2Qs(),
          isActive: true,
          state: StepState.indexed
        ),

        Step(
          title: Text("Criterion 3"),
          content: _criterion3Qs(),
          isActive: true,
          state: StepState.indexed
        ),
        Step(
          title: Text("Criterion 4"),
          content: Text("Criterion 1 questions"),
          isActive: true,
          state: StepState.indexed
        ),
        Step(
          title: Text("Belief after Evaluation"),
          content: Text("Criterion 1 questions"),
          isActive: true,
          state: StepState.indexed
        ),
        Step(
          title: Text("Alternative Thought"),
          content: Text("Criterion 1 questions"),
          isActive: true,
          state: StepState.indexed
        ),
        Step(
          title: Text("Alt Thought: Criterion 1"),
          content: Text("Criterion 1 questions"),
          isActive: true,
          state: StepState.indexed
        ),
        Step(
          title: Text("Alt Thought: Criterion 2"),
          content: Text("Criterion 1 questions"),
          isActive: true,
          state: StepState.indexed
        ),
        Step(
          title: Text("Alt Thought: Criterion 3"),
          content: Text("Criterion 1 questions"),
          isActive: true,
          state: StepState.indexed
        ),
        Step(
          title: Text("Alt Thought: Criterion 4"),
          content: Text("Criterion 1 questions"),
          isActive: true,
          state: StepState.indexed
        ),
        Step(
          title: Text("Alt Thought: Belief"),
          content: Text("Criterion 1 questions"),
          isActive: true,
          state: StepState.indexed
        ),

        
 
];




  }

    @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  void answerhandler(String question, String value){
  setState(() {
   question = value; 
  });
}

void handlerv1(String v){
  setState(() {
   c1q1 = v; 
  });
}
void handlerv2(String v){
  setState(() {
   c1q2 = v; 
  });
}
void handlerv3(String v){
  setState(() {
   c1q3 = v; 
  });
}

String 
c1q1, c1q2, c1q3, c1q4,
c2q1, c2q2, c2q3,
c3q1,c3q2,c3q3,
c4q1,c4q2,c4q3, 
thought, thoughtBelief, thoughtBelief2, altThought, altThoughtBelief
= '';



  Widget _situationQs() => Container(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text("Info"),
        Padding(padding: EdgeInsets.all(8.0),),
        Divider(height: 5.0, color: Colors.black,),
        Padding( padding: EdgeInsets.all(8.0),),
        Text("Describe the situation"),
        TextFormField(
          keyboardType: TextInputType.multiline,
          maxLines: null,
          decoration: InputDecoration(labelText: 'Situation'),
        ),
        Padding( padding: EdgeInsets.all(8.0),),
        Text("Describe your thought during the situation exactly as you had it."),
        TextFormField(
          keyboardType: TextInputType.multiline,
          maxLines: null,
          decoration: InputDecoration(labelText: 'Thought'),
        ),
        Padding( padding: EdgeInsets.all(8.0),),
        Text("How much do you believe in that thought?"),
        TextFormField(
          keyboardType: TextInputType.number,
          maxLength: 5,
          maxLengthEnforced: true,
          
        )
      ],
    ),
  );

 Widget _criterion1Qs() => 
   
  Container(
  child: Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Text("Info"),
      Padding(padding: EdgeInsets.all(8.0),),
      Divider(height: 5.0, color: Colors.black,),
      Padding( padding: EdgeInsets.all(8.0),),
      Text("Is your thought an objective statement?"),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Radio(
            value: "Yes",
            groupValue: c1q1,
            onChanged: handlerv1
          ),
          Text("Yes"),
          Radio(
            value: "No",
            groupValue: c1q1,
            onChanged: handlerv1

          ),
          Text("No")
        ],
      ),
      Padding( padding: EdgeInsets.all(8.0),),
      Text("Have I thought in this way when this happened to me before?"),
       Row(
        
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Radio(
            value: "Yes",
            groupValue: c1q2,
            onChanged: (String value) => c1q2 = value

          ),
          Text("Yes"),
          Radio(
            value: "No",
            groupValue: c1q2,
            onChanged: (String value) => c1q2 = value

          ),
          Text("No")

        ],
      ),
      Padding( padding: EdgeInsets.all(8.0),),
      Text("What would I say if a friend told me about this?"),
      TextFormField(
          keyboardType: TextInputType.multiline,
          maxLines: null,
          decoration: InputDecoration(labelText: 'Answer'),
          onSaved: (String value) => c1q3 = value,
        ),
    ],
  ),
);

 

 Widget _criterion2Qs() =>
 Container(
  child: Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Text("Info"),
      Padding(padding: EdgeInsets.all(8.0),),
      Divider(height: 5.0, color: Colors.black,),
      Padding( padding: EdgeInsets.all(8.0),),
      Text("What emotions does this thought make you feel?"),
      TextFormField(
          keyboardType: TextInputType.multiline,
          maxLines: null,
          decoration: InputDecoration(labelText: 'Answer'),
          onSaved: (String value) => c2q1 = value,
        ),
      Padding( padding: EdgeInsets.all(8.0),),
      Text("Can you control these emotions?"),  
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Radio(
            value: "Yes",
            groupValue: c2q2,
            onChanged: (String value) => c2q2 = value,

          ),
          Text("Yes"),
          Radio(
            value: "No",
            groupValue: c2q2,
            onChanged: (String value) => c2q2 = value,

          ),
          Text("No")
        ],
      ),
      Padding( padding: EdgeInsets.all(8.0),),
      Text("Can you stop thinking about it whenever you want?"),
       Row(
        
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Radio(
            value: "Yes",
            groupValue: c2q3,
            onChanged: (String value) => c2q3 = value

          ),
          Text("Yes"),
          Radio(
            value: "No",
            groupValue: c2q3,
            onChanged: (String value) => c2q3 = value,

          ),
          Text("No")

        ],
      ),
    ],
  ),
);

Widget _criterion3Qs() =>
Container(
  child: Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Text("Info"),
      Padding(padding: EdgeInsets.all(8.0),),
      Divider(height: 5.0, color: Colors.black,),
      Padding( padding: EdgeInsets.all(8.0),),
      Text("How does thinking like this help you in any way?"),
      TextFormField(
          keyboardType: TextInputType.multiline,
          maxLines: null,
          decoration: InputDecoration(labelText: 'Answer'),
          onSaved: (String value) => c3q1 = value,
        ),
      Padding( padding: EdgeInsets.all(8.0),),
      Text("Does it help me solve problems or does it create more?"),  
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Radio(
            value: "Solves",
            groupValue: c3q2,
            onChanged: (String value) => c3q2 = value,

          ),
          Text("Solves Problems"),
          Radio(
            value: "Creates",
            groupValue: c3q2,
            onChanged: (String value) => c3q2 = value,

          ),
          Text("Creates More")
        ],
      ),
      Padding( padding: EdgeInsets.all(8.0),),
      Text("Do you get what you want by thinking this way?"),
       Row(
        
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Radio(
            value: "Yes",
            groupValue: c2q3,
            onChanged: (String value) => c3q3 = value

          ),
          Text("Yes"),
          Radio(
            value: "No",
            groupValue: c2q3,
            onChanged: (String value) => c3q3 = value,

          ),
          Text("No")

        ],
      ),
    ],
  ),
);

Widget _criterion4Qs() =>
Container();

 




  @override
  Widget build(BuildContext context){
        void showSnackBarMessage(String message,
          [MaterialColor color = Colors.red]) {
        Scaffold
            .of(context)
            .showSnackBar(new SnackBar(content: new Text(message)));
      }

    void _saveLog(){
      final FormState formState = _formKey.currentState;
      if (!formState.validate()) {
          showSnackBarMessage('Please enter correct data');
        } else {
          formState.save();
          print("haha yes");
        }
    }
    return Container(
    
      child: Form(
        key: _formKey,
        child: 
        
        new ListView(
          
          shrinkWrap: true,
          children: <Widget>[
          Stepper(
            physics: ClampingScrollPhysics(),
                type: StepperType.vertical,
                currentStep: this._index,
                steps: steps,
                onStepContinue: (){
                  setState(() {
                   if (_index < steps.length - 1){
                     _index = _index + 1;
                   } else{
                     _index = 0;
                   }
                  });
                },
                onStepCancel: (){
                  setState(() {
                   if (_index > 0){
                     _index = _index - 1;
                   } else{
                     _index = 0;
                   }  
                  });
                },
                onStepTapped: (step) {
                  setState(() {
                   _index = step; 
                  });
                },
              ),
            new RaisedButton(
              child: new Text("Save", style: new TextStyle(color:Colors.white),
              ),
              onPressed: _saveLog,
              color: Colors.blue,

            )
        ],),
      ),
      
    );
  }
}