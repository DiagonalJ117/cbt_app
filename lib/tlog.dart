import 'package:flutter/material.dart';
import 'package:cbt_app/questions.dart';


class ThoughtLog extends StatefulWidget {
  @override
  _ThoughtLogState createState() => _ThoughtLogState();
}

class _ThoughtLogState extends State<ThoughtLog>{

 Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Thought Evaluation"),
      ),
      body: LogBody()
    );
  }
}

