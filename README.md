# CBT App

Aplicación móvil para terapia cognitivo conductual hecha en Flutter. Basada en los criterios de racionalidad vistos en el libro "Tratamiento Psicológico del Pánico-Agorafobia." por Carmen Pastor y Juan Sevillá (2003).

<img src="http://cetecova.com/wp-content/uploads/libro_panico.jpg" align="center">


## Hecha con

Flutter

<img src="https://venturebeat.com/wp-content/uploads/2019/02/google-flutter-logo-white.png" width="150" align="center">